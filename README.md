# first-steps

## How to initialize this project

```bash
go mod init gitlab.com/path/to/your/repository/first-steps
# create a file named main.go
touch main.go
# generate wasm_exec.js
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .
```

## Compilation

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

## Test the page

```bash
# serve
python3 -m http.server
```