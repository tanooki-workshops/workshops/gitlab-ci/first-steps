package main

import (
	"fmt"
	"syscall/js"
)

// HelloWorld returns a JavaScript function
func HelloWorld() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// Return a JS dictionary
		return map[string]interface{}{
			"message":  "👋 Hello World 🌍",
			"author": "@k33g_org",
		}
	})
}

func main() {
	fmt.Println("Hello From GoLang 😃")

	// Define the functions in the JavaScript scope
	js.Global().Set("HelloWorld", HelloWorld())

	//select {}
	<-make(chan bool)
}
